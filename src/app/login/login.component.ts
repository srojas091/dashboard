import { Component, OnInit } from '@angular/core';
import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private token;

  constructor(private socialAuthService: SocialAuthService, 
              private userService: UserService,
              private router: Router,
              private localStorageService: LocalStorageService,
              iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) { 
    iconRegistry.addSvgIcon('google', sanitizer.bypassSecurityTrustResourceUrl('../../assets/Google.svg'));
  }

  ngOnInit() {
    this.token = this.localStorageService.getLocalStorage('Token');
    if(this.token && Date.now() < parseInt(this.token['expiresAt'], 10)) {
      this.router.navigate(['/dashboard']);
    }
    
    this.socialAuthService.authState.subscribe(async (user) => {
      if(user){
        const token = await this.userService.loginUser(user);
        this.localStorageService.setLocalStorage('Token', token);
        this.router.navigate(['/dashboard']);
      }
    });
  }

  signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.localStorageService.setLocalStorage('Token', '');
    this.socialAuthService.signOut();
  }
  
}
