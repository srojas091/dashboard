import { Component, Inject, OnInit } from '@angular/core';
import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { LocalStorageService } from '../services/local-storage.service';
import { FormControl, Validators } from '@angular/forms';
import { LaCajaService } from '../services/lacaja.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  public pin = new FormControl('', [Validators.required]);
  public email = new FormControl('', [Validators.email, Validators.required]);
  public championshioInfo;

  constructor(private laCajaService: LaCajaService,
              private _snackBar: MatSnackBar,
              @Inject(DOCUMENT) private document: Document) { }

  ngOnInit(): void {
  }

  getErrorMessage(type: string) {
    if (type === 'pin' && this.pin.hasError('required')) {
      return 'Debe ingresar el pin';
    }
    if (type === 'email' && this.email.errors) {
      return 'No es un email válido';
    }
  }

  async getChampionshipInfo(){
    if (this.pin.errors || this.email.errors) { return; }
    const RESPONSE = await this.laCajaService.getChampionshipByPinEmail(this.pin.value, this.email.value);
    if (RESPONSE) {
      this.document.location.href = `${RESPONSE.url}?hash=${RESPONSE.hash}&email=${RESPONSE.email}`;
    } else {
      this.championshioInfo = undefined;
      let snackText = 'Datos inválidos';
      this._snackBar.open(snackText, '', {
        duration: 5 * 1000,
      });
    }
  }  
}
