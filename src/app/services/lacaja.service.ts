import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpOptions } from '../entities/http-options';
import { SocialUser } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class LaCajaService {

  constructor(private httpService: HttpService) { }

  async getChampionshipByPin(pin: string){
    try {
      const url = `https://service.lacajaludika.com/championship/pin/${pin}`;
      const options = new HttpOptions(url, 'GET');
      return await this.httpService.makeCall(options);
    } catch (error) {
      console.log(error);
    }
  }

  async getChampionshipByPinEmail(pin: string, email: string){
    try {
      const url = `https://service.lacajaludika.com/championship/pin/${pin}/${email}`;
      const options = new HttpOptions(url, 'GET');
      return await this.httpService.makeCall(options);
    } catch (error) {
      console.log(error);
    }
  }
}
