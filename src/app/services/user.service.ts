import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpOptions } from '../entities/http-options';
import { SocialUser } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpService: HttpService) { }

  async loginUser(user: SocialUser){
    const url = `https://service.lacajaludika.com/auth/login`;
    const options = new HttpOptions(url, 'POST');
    options.body(user);
    return await this.httpService.makeCall(options);
  }
}
