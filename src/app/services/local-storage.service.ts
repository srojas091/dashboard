import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  public getLocalStorage(key: string): any {
    try {
      const value = localStorage.getItem(key) || '';
      return JSON.parse(value); 
    } catch (error) {
      return null;
    }
  }

  public setLocalStorage(key: string, value: string): boolean {
    try {
      localStorage.setItem(key,  JSON.stringify(value));
      return true;
    } catch (error) {
      return false;
    }
  }
}
