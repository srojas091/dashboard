export class HttpOptions {
    url: string;
    method: "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
    auth: boolean;
    errorPage: string;
    bodyData: any;
    responseType: "arraybuffer" | "blob" | "json" | "text";
    private headers: {key: string, value: string}[] = [];
  
    constructor(
      url: string = "",
      method: "GET" | "POST" | "PUT" | "PATCH" | "DELETE" = "GET",
      auth: boolean = true,
      errorPage: string = '',
      responseType: "arraybuffer" | "blob" | "json" | "text" = "json"
    ) {
      this.url = url;
      this.method = method;
      this.auth = auth;
      this.errorPage = errorPage;
      this.responseType = responseType;
    }
  
    body(bodyData: any) {
      if (
        this.method.toUpperCase() === "GET" ||
        this.method.toUpperCase() === "DELETE"
      ) {
        const aux = [];
        for (const param in bodyData) {
          if (bodyData[param] !== undefined && bodyData[param] !== "") {
            aux.push([param, bodyData[param]].join("="));
          }
        }
        this.url = this.url + "?" + aux.join("&");
      } else {
        this.bodyData = bodyData;
      }
    }
  
    addHeader(key: string, value: string) {
      this.headers.push({ key, value });
    }
  
    getHeaders() {
      return this.headers;
    }
  }
  