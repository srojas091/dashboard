import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingGamesComponent } from './pages/initial-setup/setting-games/setting-games.component';
import { AddPlayersComponent } from './pages/initial-setup/add-players/add-players.component';
import { GenerateTeamsComponent } from './pages/initial-setup/generate-teams/generate-teams.component';
import { SelectGamesComponent } from './pages/initial-setup/select-games/select-games.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { ChampionshipComponent } from './championship/championship.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'pin', component: ChampionshipComponent},
  { path: 'ingreso', component: LandingComponent},
  { path: 'configuracion', component: SettingGamesComponent, canActivate: [AuthGuard] },
  { path: 'agregar-jugadores', component: AddPlayersComponent, canActivate: [AuthGuard] },
  { path: 'generar-equipos', component: GenerateTeamsComponent, canActivate: [AuthGuard] },
  { path: 'seleccionar-juegos', component: SelectGamesComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: 'pin', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
