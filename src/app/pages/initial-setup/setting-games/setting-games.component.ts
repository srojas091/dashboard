import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-setting-games',
  templateUrl: './setting-games.component.html',
  styleUrls: ['./setting-games.component.scss']
})
export class SettingGamesComponent implements OnInit {

  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) { 
    this.form = this.formBuilder.group({
      numberPlayers: '',
      numberTeams: '',
      startDates: '',
      endDates: '',
    });
  }

  ngOnInit(): void { }

  onSave() {
    const form = {
      numberPlayers: this.form.get('numberPlayers')?.value,
      numberTeams: this.form.get('numberTeams')?.value,
      startDates: this.form.get('startDates')?.value,
      endDates: this.form.get('endDates')?.value
    }
    console.log(form)
  }

}
