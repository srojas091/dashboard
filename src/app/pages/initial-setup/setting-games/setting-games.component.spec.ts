import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingGamesComponent } from './setting-games.component';

describe('SettingGamesComponent', () => {
  let component: SettingGamesComponent;
  let fixture: ComponentFixture<SettingGamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingGamesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
