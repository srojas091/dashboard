import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectGamesComponent } from './select-games.component';

describe('SelectGamesComponent', () => {
  let component: SelectGamesComponent;
  let fixture: ComponentFixture<SelectGamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectGamesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
