import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SettingGamesComponent } from './pages/initial-setup/setting-games/setting-games.component';
import { AddPlayersComponent } from './pages/initial-setup/add-players/add-players.component';
import { GenerateTeamsComponent } from './pages/initial-setup/generate-teams/generate-teams.component';
import { SelectGamesComponent } from './pages/initial-setup/select-games/select-games.component';
import { HeaderComponent } from './shared/header/header.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HttpService } from './services/http.service';
import { GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { LandingComponent } from './landing/landing.component';
import { ChampionshipComponent } from './championship/championship.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SettingGamesComponent,
    AddPlayersComponent,
    GenerateTeamsComponent,
    SelectGamesComponent,
    HeaderComponent,
    DashboardComponent,
    ChampionshipComponent,
    LandingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SocialLoginModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [
    HttpService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '777968968711-79633clpilkhc67ng0ltes299l3finhm.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
