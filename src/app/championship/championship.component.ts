import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LaCajaService } from '../services/lacaja.service';

@Component({
  selector: 'app-championship',
  templateUrl: './championship.component.html',
  styleUrls: ['./championship.component.scss']
})
export class ChampionshipComponent implements OnInit {
  public pin = new FormControl('', [Validators.required]);
  public championshioInfo;

  constructor(private laCajaService: LaCajaService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  getErrorMessage() {
    if (this.pin.hasError('required')) {
      return 'Debe ingresar el pin';
    }

    return this.pin.hasError('pin') ? 'No es un pin válido' : '';
  }

  async getChampionshipInfo(){
    if (this.pin.hasError('required')) { return; }
    const RESPONSE = await this.laCajaService.getChampionshipByPin(this.pin.value);
    console.log('RESPONSE', RESPONSE);
    if (RESPONSE) {
      this.championshioInfo = RESPONSE;
    } else {
      this.championshioInfo = undefined;
      let snackText = 'Pin inválido';
      this._snackBar.open(snackText, '', {
        duration: 5 * 1000,
      });
    }
  }
}
